package com.prsen.TicketsManagement.domain.cinema

import java.util.UUID

import com.prsen.TicketsManagement.client.ContactInfo
import com.prsen.TicketsManagement.domain.cinema.reservation.Reservation

case class Cinema(name: String, id: UUID = UUID.randomUUID()) {

  private val seats: Seats = new Seats(20, 30)

  def getAvailableSeats: Traversable[Seat] = seats.getAvailableSeats

  def reserveSeat(lineNr: Int, seatNr: Int, owner: ContactInfo): Reservation = seats.reserveSeat(lineNr, seatNr, owner)

  def cancelSeatReservation(reservationId: UUID): Unit = seats.unreserveSeat(reservationId)
}
