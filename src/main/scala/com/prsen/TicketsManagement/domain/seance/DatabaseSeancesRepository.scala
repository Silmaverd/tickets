package com.prsen.TicketsManagement.domain.seance

import java.util.UUID

import com.prsen.TicketsManagement.domain.Repository
import org.springframework.stereotype

@stereotype.Repository
class DatabaseSeancesRepository extends Repository[Seance] {

    override def get(id: UUID): Option[Seance] = ???

    override def insert(elem: Seance): Unit = ???

    override def remove(id: UUID): Unit = ???
}
