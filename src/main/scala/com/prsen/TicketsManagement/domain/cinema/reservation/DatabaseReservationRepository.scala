package com.prsen.TicketsManagement.domain.cinema.reservation

import java.util.UUID

import com.prsen.TicketsManagement.domain.Repository

class DatabaseReservationRepository extends Repository[Reservation] {
    override def get(id: UUID): Option[Reservation] = ???

    override def insert(elem: Reservation): Unit = ???

    override def remove(id: UUID): Unit = ???
}
