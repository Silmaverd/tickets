package com.prsen.TicketsManagement.domain.price

import com.prsen.TicketsManagement.admin.SetTicketPriceRequest

object Price {
  def fromRequest(setTicketPriceRequest: SetTicketPriceRequest) = Price(setTicketPriceRequest.price)
}

case class Price(value: Double)
