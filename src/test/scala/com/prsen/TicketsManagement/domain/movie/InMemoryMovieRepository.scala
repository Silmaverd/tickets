package com.prsen.TicketsManagement.domain.movie

import java.util.UUID

import com.prsen.TicketsManagement.domain.Repository

import scala.collection.mutable

class InMemoryMovieRepository extends Repository[Movie] {

    private val memory = mutable.Map[UUID, Movie]()

    override def get(id: UUID): Option[Movie] = memory.get(id)

    override def insert(elem: Movie): Unit = memory.put(elem.id, elem)

    override def remove(id: UUID): Unit = memory.remove(id)
}
