package com.prsen.TicketsManagement.domain.cinema

import com.prsen.TicketsManagement.client.{ContactInfo, EmailAddress, PhoneNumber}
import org.junit.runner.RunWith
import org.scalatest.{FreeSpec, Matchers}
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SeatsSpec extends FreeSpec with Matchers {


    "given a default cinema audience" - {
        val exampleCinema = Cinema("test")
        val exampleCustomer = ContactInfo("test", "test", PhoneNumber("test"), EmailAddress("test"))

        "all seats are available by default" in {
            exampleCinema.getAvailableSeats.size shouldBe 600
        }

        "reserved seat is not available" in {
            exampleCinema.reserveSeat(1, 1, exampleCustomer)
            exampleCinema.getAvailableSeats should not contain Seat(1, 1)
        }

        "unreserved seat is available again" in {
            val reservation = exampleCinema.reserveSeat(2, 2, exampleCustomer)
            exampleCinema.cancelSeatReservation(reservation.id)
            exampleCinema.getAvailableSeats should contain(Seat(2, 2))
        }
    }
}
