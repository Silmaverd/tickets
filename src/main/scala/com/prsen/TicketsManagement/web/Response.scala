package com.prsen.TicketsManagement.web

case class Response[T <: View](code: Int, errorMessage: Option[String], payload: Option[T])

