package com.prsen.TicketsManagement.admin

import java.util.{Date, UUID}

import com.prsen.TicketsManagement.domain.cinema.{Cinema, InMemoryCinemaRepository}
import com.prsen.TicketsManagement.domain.movie.{InMemoryMovieRepository, Movie}
import com.prsen.TicketsManagement.domain.seance.InMemorySeanceRepository
import org.junit.runner.RunWith
import org.scalatest.{FreeSpec, Matchers}
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SeanceHandlerSpec extends FreeSpec with Matchers {

  val movie = exampleMovie
  val date = new Date()
  val cinema = exampleCinema

  "when adding a seance" - {

    val movieRepo = new InMemoryMovieRepository
    val seanceRepo = new InMemorySeanceRepository
    val cinemaRepo = new InMemoryCinemaRepository
    val seanceHandler = new SeanceHandler(movieRepo, seanceRepo, cinemaRepo)

    cinemaRepo.insert(cinema)
    movieRepo.insert(movie)
    seanceHandler.addSeance(AddSeanceRequest(movie.id, date, cinema.id))

    "seance data is persisted" in {
      seanceRepo.getAll.size shouldBe 1
    }

    "movie data is updated" in {
      movieRepo.get(movie.id).get.seances.size shouldBe 1
    }

    "cannot add seance of non-existant movie" in {
      an [IllegalArgumentException] shouldBe thrownBy {
        seanceHandler.addSeance(AddSeanceRequest(UUID.randomUUID(), date, cinema.id))
      }
    }

    "cannot add seance of non-existant cinema" in {
      an [IllegalArgumentException] shouldBe thrownBy {
        seanceHandler.addSeance(AddSeanceRequest(movie.id, date, UUID.randomUUID()))
      }
    }
  }

  "seance" - {

    val movieRepo = new InMemoryMovieRepository
    val seanceRepo = new InMemorySeanceRepository
    val cinemaRepo = new InMemoryCinemaRepository
    val seanceHandler = new SeanceHandler(movieRepo, seanceRepo, cinemaRepo)

    val addSeanceRequest = AddSeanceRequest(movie.id, date, cinema.id)

    cinemaRepo.insert(cinema)
    movieRepo.insert(movie)
    seanceHandler.addSeance(addSeanceRequest)

    val seance = seanceRepo.getAll.head

    "can have normal ticket price be set" in {
      val request = SetTicketPriceRequest(seance.id, 30.0f)
      seanceHandler.defineNormalTicketPrice(request)
      seanceHandler.getSeance(seance.id).get.normalTicketPrice.get.value shouldBe 30.0f
    }

    "can have concession ticket price be set" in {
      val request = SetTicketPriceRequest(seance.id, 30.0f)
      seanceHandler.defineConcessionTicketPrice(request)
      seanceHandler.getSeance(seance.id).get.concessionTicketPrice.get.value shouldBe 30.0f
    }
  }

  private def exampleMovie: Movie = Movie(UUID.randomUUID(), "test")

  private def exampleCinema: Cinema = Cinema("test", UUID.randomUUID())

}
