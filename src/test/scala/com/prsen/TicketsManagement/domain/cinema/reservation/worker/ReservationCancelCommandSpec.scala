package com.prsen.TicketsManagement.domain.cinema.reservation.worker

import java.text.SimpleDateFormat
import java.util.UUID

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FreeSpec, Matchers}

@RunWith(classOf[JUnitRunner])
class ReservationCancelCommandSpec extends FreeSpec with Matchers {

    private val dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm")

    "command should be executed" - {

        val command = ReservationCancelCommand(dateFormatter.parse("2019-06-22 11:00"), UUID.randomUUID())

        "if current date is after closure date" in {
            command.shouldExecute() shouldBe true
        }
    }
}
