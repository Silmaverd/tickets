package com.prsen.TicketsManagement.cashier

import java.util.UUID

import com.prsen.TicketsManagement.TestableSystem
import com.prsen.TicketsManagement.domain.cinema.reservation.Reservation
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FreeSpec, Matchers}

@RunWith(classOf[JUnitRunner])
class CashierServiceSpec extends FreeSpec with Matchers {

    val system = new TestableSystem

    "given reservation id" - {
        val id = UUID.randomUUID()
        system.reservationRepo.insert(Reservation(id = id, owner = system.exampleContactInfo))
        val cashierService = new CashierService(system.seanceHandler, system.cinemaRepo, system.reservationRepo)

        "marks reservation as paid" in {
            cashierService.markAsPaid(id)
            system.reservationRepo.get(id).get.isPaid shouldBe true
        }

        "fails if there is no reservation with given id" in {
            val randomId = UUID.randomUUID()
            an [IllegalArgumentException] shouldBe thrownBy(cashierService.markAsPaid(randomId))
        }
    }

    "before seance cashier can" - {

        val cinema = system.createExampleCinema
        val seance = system.createExampleSeance(cinema.id)
        val contactInfo = system.exampleContactInfo
        system.seanceRepo.insert(seance)
        val cashierService = new CashierService(system.seanceHandler, system.cinemaRepo, system.reservationRepo)

        "make paid reservation of normal tickets" in {
            val reservation = cashierService.makePaidReservationNormal(seance.id, 2, 2, contactInfo)
            reservation.isPaid shouldBe true
        }

        "make paid reservation of concession tickets" in {
            val reservation = cashierService.makePaidReservationConcession(seance.id, 3, 3, contactInfo)
            reservation.isPaid shouldBe true
        }
    }
}
