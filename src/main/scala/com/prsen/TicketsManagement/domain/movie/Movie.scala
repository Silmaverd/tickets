package com.prsen.TicketsManagement.domain.movie

import java.util.UUID

import com.prsen.TicketsManagement.domain.seance.Seance

case class Movie(id: UUID = UUID.randomUUID(), title: String, seances: Set[Seance] = Set.empty)
