package com.prsen.TicketsManagement.domain.cinema

import java.util.UUID

import com.prsen.TicketsManagement.domain.Repository
import org.springframework.stereotype

@stereotype.Repository
class DatabaseCinemaRepository extends Repository[Cinema] {
    override def get(id: UUID): Option[Cinema] = ???

    override def insert(elem: Cinema): Unit = ???

    override def remove(id: UUID): Unit = ???
}
