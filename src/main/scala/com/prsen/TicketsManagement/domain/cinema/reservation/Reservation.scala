package com.prsen.TicketsManagement.domain.cinema.reservation

import java.util.UUID

import com.prsen.TicketsManagement.client.ContactInfo
import com.prsen.TicketsManagement.domain.price.Price

case class Reservation(id: UUID = UUID.randomUUID(), owner: ContactInfo, price: Option[Price] = None, isPaid: Boolean = false)
