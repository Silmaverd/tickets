package com.prsen.TicketsManagement.domain.cinema

import java.util.UUID

import com.prsen.TicketsManagement.client.ContactInfo
import com.prsen.TicketsManagement.domain.cinema.reservation.Reservation

private[cinema] class Seats(lines: Int, seatsEachLine: Int) {

    private case class SeatLine(number: Int, seats: List[Seat])

    private val seatLines = (1 to lines).toList.map(lineNumber => {
        SeatLine(lineNumber, (1 to seatsEachLine).toList.map(seatNumber => Seat(lineNumber, seatNumber)))
    })

    def reserveSeat(lineNumber: Int, seatNumber: Int, owner: ContactInfo) = {
        val reservation = Reservation(owner = owner)
        val seat = assertSeat(lineNumber, seatNumber)

        if (seat.reservation.isDefined)
            throw new IllegalArgumentException(s"Seat ($lineNumber, $seatNumber) is already reserved")
        else
            seat.reservation = Some(reservation)
        reservation
    }

    def unreserveSeat(reservationNumber: UUID): Unit = {
        val maybeReservation =
            seatLines
                .flatMap(_.seats)
                .filter(_.reservation.isDefined)
                .find(_.reservation.get.id == reservationNumber)

        maybeReservation.orElse(throw new IllegalArgumentException(s"There is no reservation $reservationNumber in this cinema"))
            .foreach(_.reservation = None)
    }

    def getAvailableSeats = seatLines.flatMap(_.seats).filter(_.reservation.isEmpty)

    private def assertSeat(lineNumber: Int, seatNumber: Int): Seat = {
        if (lineNumber > lines || seatNumber > seatsEachLine)
            throw new IllegalArgumentException(s"There is no seat with number ($lineNumber, $seatNumber)")
        else {
            seatLines(lineNumber - 1).seats(seatNumber - 1)
        }
    }
}
