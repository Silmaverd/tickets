package com.prsen.TicketsManagement.domain.seance

import java.util.{Date, UUID}

import com.prsen.TicketsManagement.domain.price.Price

case class Seance(id: UUID = UUID.randomUUID(),
                  date: Date,
                  cinemaId: UUID,
                  normalTicketPrice: Option[Price] = None,
                  concessionTicketPrice: Option[Price] = None)
