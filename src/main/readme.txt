In order to compile and run the project you should:
 - install Scala 2.11.8
 - add scala library to main module in project settings
 - run maven clean + install

In order to run the test you should follow the previous steps, and
 - right-click on module, run -> all tests