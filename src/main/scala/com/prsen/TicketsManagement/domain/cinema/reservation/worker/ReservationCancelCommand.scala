package com.prsen.TicketsManagement.domain.cinema.reservation.worker

import java.util.{Date, UUID}

case class ReservationCancelCommand(cancelDate: Date, reservationId: UUID) {

    def shouldExecute(): Boolean = {
        new Date().after(cancelDate)
    }
}
