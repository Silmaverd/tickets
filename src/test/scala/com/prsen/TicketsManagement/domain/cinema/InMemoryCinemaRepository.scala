package com.prsen.TicketsManagement.domain.cinema

import java.util.UUID

import com.prsen.TicketsManagement.domain.Repository

import scala.collection.mutable

class InMemoryCinemaRepository extends Repository[Cinema] {

    private val memory = mutable.Map[UUID, Cinema]()

    override def get(id: UUID): Option[Cinema] = memory.get(id)

    override def insert(elem: Cinema): Unit = memory.put(elem.id, elem)

    override def remove(id: UUID): Unit = memory.remove(id)
}
