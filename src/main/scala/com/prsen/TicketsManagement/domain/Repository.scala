package com.prsen.TicketsManagement.domain

import java.util.UUID

trait Repository[T] {

    def get(id: UUID): Option[T]

    def insert(elem: T): Unit

    def remove(id: UUID): Unit

    def update(id: UUID, func: T => T): Unit = {
        val elem = get(id).getOrElse(throw new IllegalArgumentException(s"No element of index $id"))
        remove(id)
        insert(
            func(elem)
        )
    }
}
