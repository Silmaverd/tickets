package com.prsen.TicketsManagement.web

import org.json4s.{DefaultFormats, DefaultJsonFormats, Formats}
import org.json4s.jackson.Serialization.write


object json {

    implicit val formats: Formats = DefaultFormats

    def serializeToJson[T <: View](obj: T) = {
        write(obj)(formats)
    }
}
