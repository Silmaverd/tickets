package com.prsen.TicketsManagement.domain.cinema.reservation.worker

import java.util.{Date, UUID}
import com.prsen.TicketsManagement.domain.Repository
import com.prsen.TicketsManagement.domain.cinema.reservation.Reservation

import scala.collection.mutable

class ReservationCancelExecutor(reservationRepository: Repository[Reservation]) {

    private var commands = new mutable.MutableList[ReservationCancelCommand]()

    def addClosureCommand(reservationId: UUID, closureDate: Date): Unit = {
        commands += ReservationCancelCommand(closureDate, reservationId)
    }

    def executeClosures(): Unit = {
        val commandsToBeClosed = new mutable.MutableList[ReservationCancelCommand]()
        commands.foreach(command => {
            if (command.shouldExecute()) {
                reservationRepository.remove(command.reservationId)
                commandsToBeClosed += command
            }
        })
        commands = commands diff commandsToBeClosed
    }
}
