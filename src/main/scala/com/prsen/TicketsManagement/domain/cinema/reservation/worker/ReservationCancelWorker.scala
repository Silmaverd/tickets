package com.prsen.TicketsManagement.domain.cinema.reservation.worker


class OrderClosureWorker(reservationClosureExecutor: ReservationCancelExecutor, var orderClosureEnabled: Boolean) extends Runnable {

    @volatile private var isRunning = false
    @volatile private var shouldStop = false

    def stop(): Unit = {
        synchronized {
            if (isRunning) {
                shouldStop = true
            }
        }
    }

    override def run(): Unit = {
        isRunning = true
        shouldStop = false
        while ( !shouldStop ) try {
            if (orderClosureEnabled) reservationClosureExecutor.executeClosures
            Thread.sleep(60 * 1000) // 1 minute

        } catch {
            case e: InterruptedException =>
                isRunning = false
                shouldStop = false
        }
    }
}

