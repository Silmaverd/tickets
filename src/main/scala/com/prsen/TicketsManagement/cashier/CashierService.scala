package com.prsen.TicketsManagement.cashier

import java.util.UUID

import com.prsen.TicketsManagement.admin.SeanceHandler
import com.prsen.TicketsManagement.client.ContactInfo
import com.prsen.TicketsManagement.domain.Repository
import com.prsen.TicketsManagement.domain.cinema.Cinema
import com.prsen.TicketsManagement.domain.cinema.reservation.Reservation
import com.prsen.TicketsManagement.domain.seance.Seance
import org.springframework.stereotype.Component

@Component
class CashierService(seanceHandler: SeanceHandler, cinemaRepository: Repository[Cinema],
                     reservationRepository: Repository[Reservation]) {

    def markAsPaid(reservationId: UUID) = {
        reservationRepository.update(reservationId, _.copy(isPaid = true))
    }

    def makePaidReservationNormal(seanceId: UUID, seatLine: Int, seatNr: Int, contactInfo: ContactInfo): Reservation = {
        val (seance, cinema) = assertSeanceAndCinema(seanceId)
        cinema.reserveSeat(seatLine, seatNr, contactInfo).copy(price = seance.normalTicketPrice, isPaid = true)
    }

    def makePaidReservationConcession(seanceId: UUID, seatLine: Int, seatNr: Int, contactInfo: ContactInfo): Reservation = {
        val (seance, cinema) = assertSeanceAndCinema(seanceId)
        cinema.reserveSeat(seatLine, seatNr, contactInfo).copy(price = seance.concessionTicketPrice, isPaid = true)
    }

    private def assertSeanceAndCinema(seanceId: UUID): (Seance, Cinema) = {
        val seance = seanceHandler.getSeance(seanceId)
            .getOrElse(throw new IllegalArgumentException(s"There is no seance with id $seanceId"))
        val cinema = cinemaRepository.get(seance.cinemaId)
            .getOrElse(throw new IllegalArgumentException(s"There is no seance with id $seanceId in cinema ${seance.cinemaId}"))
        (seance, cinema)
    }
}
