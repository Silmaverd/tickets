package com.prsen.TicketsManagement.client.api

import com.prsen.TicketsManagement.domain.cinema.Seat
import com.prsen.TicketsManagement.web.View

object SeatsView {

    def fromSeats(seats: Traversable[Seat]): SeatsView = {
        SeatsView(seats.map(fromSeat))
    }

    def fromSeat(seat: Seat): SeatView = {
        SeatView(seat.lineNumber, seat.seatNumber)
    }
}

case class SeatsView(seats: Traversable[SeatView]) extends View

case class SeatView(line: Int, number: Int) extends View
