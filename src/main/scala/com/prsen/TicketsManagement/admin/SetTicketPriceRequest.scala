package com.prsen.TicketsManagement.admin

import java.util.UUID

case class SetTicketPriceRequest(seanceId: UUID, price: Double)
