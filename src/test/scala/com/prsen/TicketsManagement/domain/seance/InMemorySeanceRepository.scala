package com.prsen.TicketsManagement.domain.seance

import java.util.UUID

import com.prsen.TicketsManagement.domain.Repository

import scala.collection.mutable

class InMemorySeanceRepository extends Repository[Seance] {

    private val memory = mutable.Map[UUID, Seance]()

    override def get(id: UUID): Option[Seance] = memory.get(id)

    override def insert(elem: Seance): Unit = memory.put(elem.id, elem)

    override def remove(id: UUID): Unit = memory.remove(id)

    def getAll = memory.values.toList
}
