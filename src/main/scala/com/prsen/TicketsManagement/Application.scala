package com.prsen.TicketsManagement

import org.slf4j.{Logger, LoggerFactory}
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class Application

object Application extends App {

    private val log : Logger = LoggerFactory.getLogger(getClass)

    SpringApplication.run(classOf[Application], args: _*)

    override def main(args: Array[String]): Unit = {
        super.main(args)
        log.info("Serving at 127.0.0.1:8080/swagger-ui.html")
    }
}
