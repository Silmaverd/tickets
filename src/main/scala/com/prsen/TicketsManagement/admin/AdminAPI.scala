package com.prsen.TicketsManagement.admin

import java.util.UUID

import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod, RequestParam, RestController}

@RestController
@RequestMapping(path = Array("/api/admin"))
class AdminAPI(adminService: AdminService) {

    import java.text.SimpleDateFormat

    private val dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm")

    @RequestMapping(path = Array("admin/cinema"), method = Array(RequestMethod.PUT))
    @ApiOperation(value = "Add available cinema")
    def addCinema(@RequestParam(name = "name") cinemaName: String): Unit =
        adminService.addCinema(cinemaName)

    @RequestMapping(path = Array("admin/movie"), method = Array(RequestMethod.PUT))
    @ApiOperation(value = "Add movie")
    def addMovie(@RequestParam(name = "title") movieTitle: String): Unit =
        adminService.addMovie(movieTitle)

    @RequestMapping(path = Array("admin/seance"), method = Array(RequestMethod.PUT))
    @ApiOperation(value = "Add seance")
    def addSeance(@RequestParam(name = "movieId") movieId: UUID,
                  @RequestParam(name = "date in yyyy-MM-dd HH:mm") dateString: String,
                  @RequestParam(name = "cinemaId") cinemaId: UUID): Unit =
        adminService.addSeance(movieId, dateFormatter.parse(dateString), cinemaId)

    @RequestMapping(path = Array("admin/normalTicketPrice"), method = Array(RequestMethod.PUT))
    @ApiOperation(value = "Define ticket price")
    def defineNormalTicketPrice(@RequestParam(name = "senaceId") seanceId: UUID,
                                @RequestParam(name = "price") price: Double) =
        adminService.defineNormalTicketPrice(seanceId, price)

    @RequestMapping(path = Array("admin/concessionTicketPrice"), method = Array(RequestMethod.PUT))
    @ApiOperation(value = "Define ticket price")
    def defineConcessionTicketPrice(@RequestParam(name = "senaceId") seanceId: UUID,
                                    @RequestParam(name = "price") price: Double) =
        adminService.defineConcessionTicketPrice(seanceId, price)
}