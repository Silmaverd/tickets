package com.prsen.TicketsManagement.cashier

import java.util.UUID

import com.prsen.TicketsManagement.client.api.ReservationView
import com.prsen.TicketsManagement.client.{ContactInfo, EmailAddress, PhoneNumber}
import com.prsen.TicketsManagement.web.json.serializeToJson
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod, RequestParam, RestController}

@RestController
@RequestMapping(path = Array("/api/cashier"))
class CashierApi(cashierService: CashierService) {

    @RequestMapping(path = Array("cashier/pay"), method = Array(RequestMethod.POST))
    @ApiOperation(value = "Mark reservation as paid")
    def markAsPaid(@RequestParam(name = "reservationId") reservationId: UUID): Unit = {
        cashierService.markAsPaid(reservationId)
    }

    @RequestMapping(path = Array("cashier/reserveNormal"), method = Array(RequestMethod.POST))
    @ApiOperation(value = "Reserve normal tickets for seance")
    def reserveNormalPaidTickets(@RequestParam(name = "seanceId") seanceId: UUID,
                             @RequestParam(name = "seatLine") seatLine: Int,
                             @RequestParam(name = "seatNumber") seatNumber: Int,
                             @RequestParam(name = "name") name: String,
                             @RequestParam(name = "surname") surname: String,
                             @RequestParam(name = "phone") phoneNr: String,
                             @RequestParam(name = "email") email: String): String = {
        val contactInfo = ContactInfo(name, surname, PhoneNumber(phoneNr), EmailAddress(email))
        val reservation = cashierService.makePaidReservationNormal(seanceId, seatLine, seatNumber, contactInfo)
        serializeToJson(ReservationView.fromReservation(reservation))
    }

    @RequestMapping(path = Array("cashier/reserveConcession"), method = Array(RequestMethod.POST))
    @ApiOperation(value = "Reserve concession tickets for seance")
    def reserveConcessionPaidTickets(@RequestParam(name = "seanceId") seanceId: UUID,
                                 @RequestParam(name = "seatLine") seatLine: Int,
                                 @RequestParam(name = "seatNumber") seatNumber: Int,
                                 @RequestParam(name = "name") name: String,
                                 @RequestParam(name = "surname") surname: String,
                                 @RequestParam(name = "phone") phoneNr: String,
                                 @RequestParam(name = "email") email: String): String = {
        val contactInfo = ContactInfo(name, surname, PhoneNumber(phoneNr), EmailAddress(email))
        val reservation = cashierService.makePaidReservationNormal(seanceId, seatLine, seatNumber, contactInfo)
        serializeToJson(ReservationView.fromReservation(reservation))
    }


}
