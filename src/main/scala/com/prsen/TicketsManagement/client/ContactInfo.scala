package com.prsen.TicketsManagement.client

case class ContactInfo(name: String, surname: String, phoneNr: PhoneNumber, email: EmailAddress)
