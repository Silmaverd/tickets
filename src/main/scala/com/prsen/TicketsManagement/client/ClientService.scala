package com.prsen.TicketsManagement.client

import java.util.{Date, UUID}

import com.prsen.TicketsManagement.admin.SeanceHandler
import com.prsen.TicketsManagement.domain.Repository
import com.prsen.TicketsManagement.domain.cinema.reservation.Reservation
import com.prsen.TicketsManagement.domain.cinema.reservation.worker.ReservationCancelExecutor
import com.prsen.TicketsManagement.domain.cinema.{Cinema, Seat}
import com.prsen.TicketsManagement.domain.seance.Seance
import org.springframework.stereotype.Component

@Component
class ClientService(seanceHandler: SeanceHandler, cinemaRepository: Repository[Cinema],
                    reservationCancelExecutor: ReservationCancelExecutor) {

    def makeReservationNormal(seanceId: UUID, seatLine: Int, seatNr: Int, contactInfo: ContactInfo): Reservation = {
        val (seance, cinema) = assertSeanceAndCinema(seanceId)
        val reservation = cinema.reserveSeat(seatLine, seatNr, contactInfo).copy(price = seance.normalTicketPrice)
        reservationCancelExecutor.addClosureCommand(reservation.id, nMinutesBeforeSeance(30, seance))
        reservation
    }

    def makeReservationConcession(seanceId: UUID, seatLine: Int, seatNr: Int, contactInfo: ContactInfo): Reservation = {
        val (seance, cinema) = assertSeanceAndCinema(seanceId)
        val reservation = cinema.reserveSeat(seatLine, seatNr, contactInfo).copy(price = seance.concessionTicketPrice)
        reservationCancelExecutor.addClosureCommand(reservation.id, nMinutesBeforeSeance(30, seance))
        reservation
    }

    def cancelReservation(seanceId: UUID, reservationId: UUID): Unit = {
        val (seance, cinema) = assertSeanceAndCinema(seanceId)
        cinema.cancelSeatReservation(reservationId)
    }

    def getAvailableSeatsForSeance(seanceId: UUID): Traversable[Seat] = {
        val (seance, cinema) = assertSeanceAndCinema(seanceId)
        cinema.getAvailableSeats
    }

    private def nMinutesBeforeSeance(n: Int, seance: Seance): Date = {
        new Date(seance.date.getTime - (1000 * 60 * n))
    }

    private def assertSeanceAndCinema(seanceId: UUID): (Seance, Cinema) = {
        val seance = seanceHandler.getSeance(seanceId)
            .getOrElse(throw new IllegalArgumentException(s"There is no seance with id $seanceId"))
        val cinema = cinemaRepository.get(seance.cinemaId)
            .getOrElse(throw new IllegalArgumentException(s"There is no seance with id $seanceId in cinema ${seance.cinemaId}"))
        (seance, cinema)
    }
}
