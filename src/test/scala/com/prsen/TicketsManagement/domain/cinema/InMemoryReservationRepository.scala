package com.prsen.TicketsManagement.domain.cinema

import java.util.UUID

import com.prsen.TicketsManagement.domain.Repository
import com.prsen.TicketsManagement.domain.cinema.reservation.Reservation

import scala.collection.mutable

class InMemoryReservationRepository extends Repository[Reservation] {

    private val memory = mutable.Map[UUID, Reservation]()

    override def get(id: UUID): Option[Reservation] = memory.get(id)

    override def insert(elem: Reservation): Unit = memory.put(elem.id, elem)

    override def remove(id: UUID): Unit = memory.remove(id)

}
