package com.prsen.TicketsManagement.domain.movie

import java.util.UUID

import com.prsen.TicketsManagement.domain.Repository
import org.springframework.stereotype

@stereotype.Repository
private[movie] class DatabaseMoviesRepository() extends Repository[Movie] {

    override def get(id: UUID): Option[Movie] = ???

    override def insert(elem: Movie): Unit = ???

    override def remove(id: UUID): Unit = ???
}
