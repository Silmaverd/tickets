package com.prsen.TicketsManagement.admin

import java.util.{Date, UUID}

case class AddSeanceRequest(movieId: UUID,
                            date: Date,
                            cinemaId: UUID,
                            normalTicketPrice: Option[Double] = None,
                            concessionTicketPrice: Option[Double] = None)
