package com.prsen.TicketsManagement

import java.util.{Date, UUID}

import com.prsen.TicketsManagement.admin.SeanceHandler
import com.prsen.TicketsManagement.client.{ClientService, ContactInfo, EmailAddress, PhoneNumber}
import com.prsen.TicketsManagement.domain.cinema.reservation.worker.EmptyReservationCancelExecutor
import com.prsen.TicketsManagement.domain.cinema.{Cinema, InMemoryCinemaRepository, InMemoryReservationRepository}
import com.prsen.TicketsManagement.domain.movie.{InMemoryMovieRepository, Movie}
import com.prsen.TicketsManagement.domain.seance.{InMemorySeanceRepository, Seance}

class TestableSystem {
    val (seanceRepo, cinemaRepo, movieRepo, reservationRepo) = (
        new InMemorySeanceRepository(),
        new InMemoryCinemaRepository(),
        new InMemoryMovieRepository(),
        new InMemoryReservationRepository()
    )

    val seanceHandler = new SeanceHandler(movieRepo, seanceRepo, cinemaRepo)
    val clientService = new ClientService(seanceHandler, cinemaRepo, new EmptyReservationCancelExecutor)

    def createExampleCinema: Cinema = {
        val cinema = Cinema("test")
        cinemaRepo.insert(cinema)
        cinema
    }

    def createExampleMovie(seances: Seance*): Movie = {
        val movie = Movie(title = "test", seances = seances.toSet)
        movieRepo.insert(movie)
        movie
    }

    def createExampleSeance(cinemaId: UUID): Seance = {
        val seance = Seance(date = new Date(), cinemaId = cinemaId)
        seanceRepo.insert(seance)
        seance
    }

    def exampleContactInfo = ContactInfo("Jan", "Kowalski", PhoneNumber("111111111"), EmailAddress("jan.kowalski@domain.com"))
}
