package com.prsen.TicketsManagement.domain.cinema.reservation.worker
import java.util.{Date, UUID}

class EmptyReservationCancelExecutor extends ReservationCancelExecutor(null) {

    override def addClosureCommand(reservationId: UUID, closureDate: Date): Unit = {}

    override def executeClosures(): Unit = {}

}
