package com.prsen.TicketsManagement.client

import com.prsen.TicketsManagement.TestableSystem
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FreeSpec, Matchers}

@RunWith(classOf[JUnitRunner])
class ClientServiceSpec extends FreeSpec with Matchers {

    val system = new TestableSystem
    val cinema = system.createExampleCinema
    val seance = system.createExampleSeance(cinema.id)
    val movie = system.createExampleMovie(seance)

    val clientService = system.clientService

    "when reservation is made" - {

        "if seat is free" - {

            val customer = system.exampleContactInfo
            val reservation = clientService.makeReservationNormal(seance.id, 1, 1, customer)

            "returns reservation for the customer" in {
                reservation.owner shouldBe customer
            }

            "seat is no longer free" in {
                clientService.getAvailableSeatsForSeance(seance.id).find(
                    seat => seat.lineNumber == 1 && seat.seatNumber == 1
                ) shouldBe None
            }
        }

        "if seat is not free" - {
            val customer = system.exampleContactInfo
            val reservation = clientService.makeReservationNormal(seance.id, 2, 2, customer)

            "reservation throws exception" in {
                an[IllegalArgumentException] shouldBe thrownBy(clientService.makeReservationNormal(seance.id, 2, 2, customer))
            }
        }
    }

    "when reservation is cancelled" - {

        val customer = system.exampleContactInfo
        val reservation = clientService.makeReservationNormal(seance.id, 3, 3, customer)
        clientService.cancelReservation(seance.id, reservation.id)

        "seat is free again" in {
            clientService.getAvailableSeatsForSeance(seance.id).exists(
                seat => seat.lineNumber == 3 && seat.seatNumber == 3
            ) shouldBe true
        }
    }
}
