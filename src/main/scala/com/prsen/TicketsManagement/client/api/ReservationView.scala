package com.prsen.TicketsManagement.client.api

import java.util.UUID

import com.prsen.TicketsManagement.domain.cinema.reservation.Reservation
import com.prsen.TicketsManagement.domain.price.Price
import com.prsen.TicketsManagement.web.View

object ReservationView {
    def fromReservation(reservation: Reservation) = {
        ReservationView(reservation.id, reservation.owner.name, reservation.owner.surname,
            reservation.owner.phoneNr.number, reservation.owner.email.address, reservation.price.getOrElse(Price(0)).value
        )
    }
}

case class ReservationView(id: UUID, name: String, surname: String, phoneNr: String, email: String, price: Double) extends View
