package com.prsen.TicketsManagement.domain.cinema

import com.prsen.TicketsManagement.domain.cinema.reservation.Reservation

case class Seat(lineNumber: Int, seatNumber: Int, var reservation: Option[Reservation] = None)
