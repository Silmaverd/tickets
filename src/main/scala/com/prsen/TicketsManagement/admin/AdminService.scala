package com.prsen.TicketsManagement.admin

import java.util.{Date, UUID}

import com.prsen.TicketsManagement.domain.cinema.Cinema
import com.prsen.TicketsManagement.domain.movie.Movie
import org.springframework.stereotype.Component

@Component
class AdminService(seanceHandler: SeanceHandler) {

    def addCinema(cinemaName: String): Unit = {
        val cinema = Cinema(name = cinemaName)
    }

    def addMovie(title: String) = {
        val movie = Movie(title = title)
    }

    def addSeance(movieId: UUID, date: Date, cinemaId: UUID) = {
        seanceHandler.addSeance(AddSeanceRequest(movieId, date, cinemaId))
    }

    def defineNormalTicketPrice(seanceId: UUID, price: Double) = {
        val request = SetTicketPriceRequest(seanceId, price)
        seanceHandler.defineNormalTicketPrice(request)
    }

    def defineConcessionTicketPrice(seanceId: UUID, price: Double) = {
        val request = SetTicketPriceRequest(seanceId, price)
        seanceHandler.defineConcessionTicketPrice(request)
    }


}
