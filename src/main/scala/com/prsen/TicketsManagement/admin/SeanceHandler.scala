package com.prsen.TicketsManagement.admin

import java.util.UUID

import com.prsen.TicketsManagement.domain.Repository
import com.prsen.TicketsManagement.domain.cinema.Cinema
import com.prsen.TicketsManagement.domain.movie.Movie
import com.prsen.TicketsManagement.domain.price.Price
import com.prsen.TicketsManagement.domain.seance.Seance
import org.springframework.stereotype.Component

@Component
class SeanceHandler(moviesRepo: Repository[Movie], seanceRepo: Repository[Seance], cinemaRepo: Repository[Cinema]) {

    def addSeance(addSeanceRequest: AddSeanceRequest) = {
        def assertCinemaExist = cinemaRepo.get(addSeanceRequest.cinemaId)
        def assertMovieExist = moviesRepo.get(addSeanceRequest.movieId)

        assertCinemaExist.getOrElse(throw new IllegalArgumentException(s"Cinema ${addSeanceRequest.cinemaId} does not exist"))
        assertMovieExist.getOrElse(throw new IllegalArgumentException(s"Movie ${addSeanceRequest.movieId} does not exist"))

        val seance = Seance(date = addSeanceRequest.date, cinemaId = addSeanceRequest.cinemaId)
        seanceRepo.insert(seance)
        moviesRepo.update(addSeanceRequest.movieId, movie => movie.copy(seances = movie.seances + seance))
    }

    def defineNormalTicketPrice(setPriceRequest: SetTicketPriceRequest) = {
        val price = Price.fromRequest(setPriceRequest)
        seanceRepo.update(setPriceRequest.seanceId, _.copy(normalTicketPrice = Some(price)))
    }

    def defineConcessionTicketPrice(setPriceRequest: SetTicketPriceRequest) = {
        val price = Price.fromRequest(setPriceRequest)
        seanceRepo.update(setPriceRequest.seanceId, _.copy(concessionTicketPrice = Some(price)))
    }

    def getSeance(id: UUID): Option[Seance] = seanceRepo.get(id)
}
