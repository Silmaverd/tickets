package com.prsen.TicketsManagement.client.api

import java.util.UUID

import com.prsen.TicketsManagement.client.{ClientService, ContactInfo, EmailAddress, PhoneNumber}
import com.prsen.TicketsManagement.web.View
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod, RequestParam, RestController}
import com.prsen.TicketsManagement.web.json._

@RestController
@RequestMapping(path = Array("/api/client"))
class ClientAPI(clientService: ClientService) {

    private case class IncomingSeat(seatLine: Int, seatNumber: Int) extends View

    @RequestMapping(path = Array("client/reserve"), method = Array(RequestMethod.POST))
    @ApiOperation(value = "Reserve normal tickets for seance")
    def reserveNormalTickets(@RequestParam(name = "seanceId") seanceId: UUID,
                             @RequestParam(name = "seatLine") seatLine: Int,
                             @RequestParam(name = "seatNumber") seatNumber: Int,
                             @RequestParam(name = "name") name: String,
                             @RequestParam(name = "surname") surname: String,
                             @RequestParam(name = "phone") phoneNr: String,
                             @RequestParam(name = "email") email: String): String = {
        val contactInfo = ContactInfo(name, surname, PhoneNumber(phoneNr), EmailAddress(email))
        val reservation = clientService.makeReservationNormal(seanceId, seatLine, seatNumber, contactInfo)
        serializeToJson(ReservationView.fromReservation(reservation))
    }

    @RequestMapping(path = Array("client/reserve"), method = Array(RequestMethod.POST))
    @ApiOperation(value = "Reserve concession tickets for seance")
    def reserveConcessionTickets(@RequestParam(name = "seanceId") seanceId: UUID,
                                 @RequestParam(name = "seatLine") seatLine: Int,
                                 @RequestParam(name = "seatNumber") seatNumber: Int,
                                 @RequestParam(name = "name") name: String,
                                 @RequestParam(name = "surname") surname: String,
                                 @RequestParam(name = "phone") phoneNr: String,
                                 @RequestParam(name = "email") email: String): String = {
        val contactInfo = ContactInfo(name, surname, PhoneNumber(phoneNr), EmailAddress(email))
        val reservation = clientService.makeReservationNormal(seanceId, seatLine, seatNumber, contactInfo)
        serializeToJson(ReservationView.fromReservation(reservation))
    }

    @RequestMapping(path = Array("client/seats"), method = Array(RequestMethod.GET))
    @ApiOperation(value = "Show available seats for seance")
    def getAvailableSeats(@RequestParam(name = "seanceId") seanceId: UUID): String = {
        val seats = clientService.getAvailableSeatsForSeance(seanceId)
        serializeToJson(SeatsView.fromSeats(seats))
    }

    @RequestMapping(path = Array("client/cancel"), method = Array(RequestMethod.POST))
    @ApiOperation(value = "Cancel reservation of tickets")
    def cancelReservation(@RequestParam(name = "seanceId") seanceId: UUID,
                          @RequestParam(name = "reservationId") reservationId: UUID): Unit = {
        clientService.cancelReservation(seanceId, reservationId)
    }

}